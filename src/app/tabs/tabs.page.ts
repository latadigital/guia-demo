import { Component, OnInit, ViewChild } from '@angular/core';
import { IonTabs } from '@ionic/angular';
import {Router} from '@angular/router';




@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage implements OnInit {

  selectTab: any;
  @ViewChild('tabs') tabs: IonTabs;

  constructor(private router: Router) { }

  ngOnInit() {
  }
  goToCategories(){
    this.router.navigate(['/tabs/categories'])
  }

  setCurrentTab(event) {
    console.log(event);    
    this.selectTab = this.tabs.getSelected();
  }
  

}